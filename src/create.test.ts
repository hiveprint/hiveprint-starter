import { fileURLToPath } from "url";
import { it, expect, vi, describe, afterAll } from "vitest";
import { execa } from "execa";
import { execSync } from "child_process";
import {
  accessTokenExists,
  addAccessTokenToNpmrc,
  askUserForAccessToken,
  askUserForConfigJsonInput,
  copyFilesFromTemplateToHivePrintPi,
  getTargetDir,
  getTemplateDir,
  setUpProject,
  updateConfigJsonWithUserInput,
  uuidKeyExistsInConfig,
} from "./utils.js";
import fs from "fs/promises";
import { HivePrintAnswers } from "./index.js";
import inquirer from "inquirer";

vi.mock("url", () => ({
  fileURLToPath: vi.fn(),
}));

vi.mock("execa");

vi.mock("child_process", () => ({
  execSync: vi.fn(),
}));

vi.mock("fs/promises");
vi.mock("inquirer");

afterAll(() => {
  vi.resetAllMocks();
});

describe("Copy files from template to hiveprint test suite", () => {
  it("getTemplateDir should return the correct template directory", () => {
    vi.mocked(fileURLToPath).mockReturnValue("/mock/target");

    const templateDir = getTemplateDir();
    expect(templateDir).toBe("/mock/templates/standard");
  });

  it("getTargetDir should return the correct target directory", () => {
    const mockCwd = "/mock/target";
    vi.spyOn(process, "cwd").mockReturnValue(mockCwd);

    const targetDir = getTargetDir();
    expect(targetDir).toBe("/mock/target/hiveprint-pi");
  });

  it("setUpProject should call as expected", async () => {
    await setUpProject();

    expect(execa).toHaveBeenCalledWith(
      "pnpm",
      ["add", "@dovetail-digital/hiveprint-pi"],
      { cwd: "./hiveprint-pi" }
    );
  });

  it("copyFilesFromTemplateToHivePrintPi should call as expected", async () => {
    await copyFilesFromTemplateToHivePrintPi();

    expect(execa).toHaveBeenCalledWith("cp", [
      "-r",
      "/mock/templates/standard",
      "/mock/target/hiveprint-pi",
    ]);
  });
});

describe("addAccessTokenToNpmrc", () => {
  it("should call execSync with the correct commands", () => {
    // Call the function with a mock access token
    const mockAccessToken = "gplat_mock-access-token";
    addAccessTokenToNpmrc(mockAccessToken);

    // Assert that execSync was called with the correct arguments
    expect(execSync).toHaveBeenCalledWith(
      `pnpm config set //gitlab.com/api/v4/projects/28900673/packages/npm/:_authToken=gplat_mock-access-token`
    );
    expect(execSync).toHaveBeenCalledWith(
      `pnpm config set @dovetail-digital:registry=https://gitlab.com/api/v4/projects/28900673/packages/npm/`
    );
  });
});

describe("accessTokenExists", () => {
  it("should call execCommand with the correct command", () => {
    // Call the function
    accessTokenExists();

    // Assert that execCommand was called with the correct arguments
    expect(execSync).toHaveBeenCalledWith(
      "pnpm config get //gitlab.com/api/v4/projects/28900673/packages/npm/:_authToken",
      { encoding: "utf8" }
    );
  });
});

describe("updateConfigJsonWithUserInput", () => {
  it("should update config.json with user input", async () => {
    // Mock data
    const mockConfig = { someExistingConfig: true };
    const mockUserInput: HivePrintAnswers = {
      printerIp: "192.168.1.1",
      serverUrl: "http://example.com",
    };

    // Mock execa to simulate reading the config file
    vi.mocked(execa).mockResolvedValue({
      // @ts-ignore
      stdout: JSON.stringify(mockConfig),
    });

    // Spy on fs.writeFile
    const writeFileSpy = vi.spyOn(fs, "writeFile").mockResolvedValue();

    // Call the function with mock input
    await updateConfigJsonWithUserInput(mockUserInput);

    // Assert execa was called correctly
    expect(execa).toHaveBeenCalledWith("cat", ["config.json"], {
      cwd: "./hiveprint-pi/config",
    });

    // Assert fs.writeFile was called with updated config
    const expectedConfig = {
      ...mockConfig,
      ip: "192.168.1.1",
      server: "http://example.com",
    };
    expect(writeFileSpy).toHaveBeenCalledWith(
      "./hiveprint-pi/config/config.json",
      JSON.stringify(expectedConfig, null, 2)
    );
  });
});

describe("ask use for things", () => {
  it("should return the access token from user input", async () => {
    const mockAccessToken = "mock-access-token";

    // Mock inquirer.prompt to simulate user input
    vi.mocked(inquirer.prompt).mockResolvedValue({
      accessToken: mockAccessToken,
    });

    // Call the function and get the result
    const accessToken = await askUserForAccessToken();

    // Assert that the function returns the mocked access token
    expect(accessToken).toBe("mock-access-token");
  });

  it("should return the config json from user input", async () => {
    vi.mocked(inquirer.prompt).mockResolvedValue({
      printerIp: "192.168.1.1",
      serverUrl: "http://example.com",
    });
    const answers = await askUserForConfigJsonInput();
    expect(answers).toEqual({
      printerIp: "192.168.1.1",
      serverUrl: "http://example.com",
    });
  });
});

describe("uuidKeyExistsInConfig", () => {
  it("should return true if uuid key exists in config", async () => {
    const mockConfigWithUuid = JSON.stringify({ uuid: "12345" });
    // @ts-ignore
    vi.mocked(execa).mockResolvedValue({ stdout: mockConfigWithUuid });

    const result = await uuidKeyExistsInConfig();
    expect(result).toBe(true);
  });

  it("should return false if uuid key does not exist in config", async () => {
    const mockConfigWithoutUuid = JSON.stringify({ someOtherKey: "value" });
    // @ts-ignore
    vi.mocked(execa).mockResolvedValue({ stdout: mockConfigWithoutUuid });

    const result = await uuidKeyExistsInConfig();
    expect(result).toBe(false);
  });

  it("should return false if there is an error reading the file", async () => {
    vi.mocked(execa).mockRejectedValue(new Error("File not found"));

    const result = await uuidKeyExistsInConfig();
    expect(result).toBe(false);
  });
});
