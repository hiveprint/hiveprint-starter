#!/usr/bin/env node

import {
  accessTokenExists,
  addAccessTokenToNpmrc,
  askUserForAccessToken,
  askUserForConfigJsonInput,
  setUpProject,
  updateConfigJsonWithUserInput,
  uuidKeyExistsInConfig,
} from "./utils.js";

(async () => {
  console.log("Setting up HivePrint starter project");
  let accessTokenArg = "";
  let printerIpArg = "";
  let printerServerUrl = "";
  process.argv.forEach(function (val, index, array) {
    if (index === 2) {
      accessTokenArg = val;
    }
    if (index === 3) {
      printerIpArg = val;
    }
    if (index === 4) {
      printerServerUrl = val;
    }
  });

  if (!(await accessTokenExists())) {
    console.log("No access token found...");
    let accessToken = "";
    if (accessTokenArg === "") {
      accessToken = await askUserForAccessToken();
    } else {
      accessToken = accessTokenArg;
    }
    addAccessTokenToNpmrc(accessToken);
  }

  if (await uuidKeyExistsInConfig()) {
    console.log(
      "Found an existing config with uuid set. This file would be in hiveprint-pi/config/config.json.\
      \n Please move this file elsewhere, re-run the command, and then move the config.json file back.\
      \n Example: mv hiveprint-pi/config/config.json ../config.json, and then later: mv ../config.json hiveprint-pi/config/config.json"
    );
    return;
  }

  await setUpProject();
  if (printerIpArg === "" || printerServerUrl === "" || accessTokenArg === "") {
    const answers = await askUserForConfigJsonInput();
    await updateConfigJsonWithUserInput(answers);
  } else {
    await updateConfigJsonWithUserInput({
      accessToken: accessTokenArg,
      printerIp: printerIpArg,
      serverUrl: printerServerUrl,
    });
  }
  console.log("Installation complete.");
})();
