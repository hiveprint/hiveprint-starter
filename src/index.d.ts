export interface HivePrintAnswers {
  accessToken?: string;
  serverUrl: string;
  printerIp: string;
}
