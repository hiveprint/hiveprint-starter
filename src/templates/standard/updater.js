import {
  checkIfUpdateIsNeeded,
  getInstalledVersionOfHivePrint,
  getLatestVersionOfHivePrint,
  getPackageUpdateChannel,
  restartApplication,
  updateApplication,
} from "./utils.js";

// Restart the application
export default function handleUpdates() {
  console.log("Checking for updates...");
  try {
    const installedVersionOfHivePrint = getInstalledVersionOfHivePrint();
    console.log("Installed version:", installedVersionOfHivePrint);

    const packageUpdateChannel = getPackageUpdateChannel();
    console.log("Package update channel:", packageUpdateChannel);

    const latestVersionOfHivePrint =
      getLatestVersionOfHivePrint(packageUpdateChannel);
    console.log("Latest version:", latestVersionOfHivePrint);

    console.log(
      `\nCurrent version: ${installedVersionOfHivePrint}
      Target version: ${latestVersionOfHivePrint}
      Pacakge update channel: ${packageUpdateChannel}`
    );

    const isUpdateNeeded = checkIfUpdateIsNeeded(
      installedVersionOfHivePrint,
      latestVersionOfHivePrint
    );

    if (!isUpdateNeeded) {
      console.log("Up to date");
      return;
    } else {
      console.log("update needed");
    }

    updateApplication(packageUpdateChannel);
    restartApplication();
  } catch (error) {
    throw new Error(`Error handling updates. ${error}`);
  }
}
