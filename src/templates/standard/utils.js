import { execSync } from "child_process";

export function getPackageUpdateChannel() {
  let packageUpdateChannel = "latest";
  if (process.env.UPDATE_CHANNEL) {
    if (process.env.UPDATE_CHANNEL === "internal") {
      return "next";
    }
    packageUpdateChannel = String(process.env.UPDATE_CHANNEL);
  }
  return packageUpdateChannel;
}

// Get installed version
export function getInstalledVersionOfHivePrint() {
  const packageName = "@dovetail-digital/hiveprint-pi";
  try {
    const stdout = execSync(`pnpm list ${packageName} --json`);
    const listOutput = JSON.parse(stdout)[0];
    return listOutput.dependencies[packageName].version;
  } catch (error) {
    throw new Error(
      `Error getting installed version for ${packageName}. ${error}`
    );
  }
}

// Get the latest version
export function getLatestVersionOfHivePrint(packageUpdateChannel) {
  let latestVersionOfHivePrint = "";
  try {
    const stdout = execSync(
      `pnpm info @dovetail-digital/hiveprint-pi dist-tags --json`
    );
    const distTagsOutput = JSON.parse(stdout);
    console.log("Dist tags output", distTagsOutput);
    latestVersionOfHivePrint = distTagsOutput[packageUpdateChannel];
    console.log("Latest version of hiveprint", latestVersionOfHivePrint);
    return latestVersionOfHivePrint;
  } catch (error) {
    throw new Error(
      `Error getting latest version for ${packageUpdateChannel}. ${error}`
    );
  }
}

export function checkIfUpdateIsNeeded(
  installedVersionOfHivePrint,
  latestVersionOfHivePrint
) {
  if (installedVersionOfHivePrint === latestVersionOfHivePrint) {
    return false;
  }
  return true;
}

export function stopApplication() {
  try {
    console.log(`Stopping application...`);
    execSync("sudo systemctl stop hiveprintpi.service");
  } catch (error) {
    throw new Error(`Error stopping the service. ${error}`);
  }
}

export function updateApplication(packageUpdateChannel) {
  try {
    console.log(`Updating application to: ${packageUpdateChannel}`);
    execSync(
      `pnpm update @dovetail-digital/hiveprint-pi@${packageUpdateChannel}`
    );
  } catch (error) {
    throw new Error(
      `Error updating package to ${packageUpdateChannel}. ${error}`
    );
  }
}

export function restartApplication() {
  console.log("Restarting application...");
  try {
    execSync("sudo systemctl restart hiveprintpi.service");
    console.log("Application restarted");
  } catch (error) {
    throw new Error(`Error restarting the service. ${error}`);
  }
}
