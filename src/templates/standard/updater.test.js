import { it, expect, vi, describe, beforeEach } from "vitest";
import { execSync } from "child_process";
import * as utils from "./utils";
import handleUpdates from "./updater";

const mockListOutput = JSON.stringify([
  {
    name: "HivePrint Starter",
    version: "1.0.0",
    path: "/Users/tylermarshall/clients/hiveprint/hiveprint-starter/src/templates/standard",
    private: false,
    dependencies: {
      "@dovetail-digital/hiveprint-pi": {
        from: "@dovetail-digital/hiveprint-pi",
        version: "0.15.0",
        resolved:
          "https://gitlab.com/api/v4/projects/28900673/packages/npm/@dovetail-digital/hiveprint-pi/-/@dovetail-digital/hiveprint-pi-0.13.2.tgz",
        path: "/Users/tylermarshall/clients/hiveprint/hiveprint-starter/src/templates/standard/node_modules/.pnpm/@dovetail-digital+hiveprint-pi@0.13.2/node_modules/@dovetail-digital/hiveprint-pi",
      },
    },
  },
]);

vi.mock("child_process", () => ({
  exec: vi.fn(),
  execSync: vi.fn(),
}));

beforeEach(() => {
  vi.restoreAllMocks();
});

describe("test getPackageUpdateChannel", () => {
  it("should get the latest package update channel with no env var", () => {
    const packageUpdateChannel = utils.getPackageUpdateChannel();
    expect(packageUpdateChannel).toBe("latest");
  });

  it("should get the latest package update channel with env var", () => {
    vi.stubEnv("UPDATE_CHANNEL", "latest");
    const packageUpdateChannel = utils.getPackageUpdateChannel();
    expect(packageUpdateChannel).toBe("latest");
  });

  it("should get the next package update channel with env var", () => {
    vi.stubEnv("UPDATE_CHANNEL", "next");
    const packageUpdateChannel = utils.getPackageUpdateChannel();
    expect(packageUpdateChannel).toBe("next");
  });

  it("should work with backwards compatible internal update channel)", () => {
    vi.stubEnv("UPDATE_CHANNEL", "internal");
    const packageUpdateChannel = utils.getPackageUpdateChannel();
    expect(packageUpdateChannel).toBe("next");
  });
});

describe("test getInstalledVersionOfHivePrint", () => {
  it("should get the installed version of hiveprint", () => {
    vi.mocked(execSync).mockImplementationOnce((command) => {
      expect(command).toBe("pnpm list @dovetail-digital/hiveprint-pi --json");
      return mockListOutput;
    });

    const installedVersionOfHivePrint = utils.getInstalledVersionOfHivePrint();
    expect(installedVersionOfHivePrint).toBe("0.15.0");
    expect(execSync).toHaveBeenCalledTimes(1);
  });

  it("should test try/catch for installed version", () => {
    vi.mocked(execSync, "execSync").mockImplementation(() => {
      throw new Error("test error");
    });
    expect(() => utils.getInstalledVersionOfHivePrint()).toThrow();
  });
});

describe("test getLatestVersionOfHivePrint", () => {
  it("should return latest version of hiveprint package", () => {
    vi.mocked(execSync).mockImplementation((command) => {
      expect(command).toBe(
        "pnpm info @dovetail-digital/hiveprint-pi dist-tags --json"
      );
      return JSON.stringify({ latest: "0.15.0", next: "0.15.1-next.2" });
    });

    expect(utils.getLatestVersionOfHivePrint("latest")).toBe("0.15.0");
    expect(utils.getLatestVersionOfHivePrint("next")).toBe("0.15.1-next.2");
  });

  it("should test try/catch for latest version", () => {
    vi.mocked(execSync, "execSync").mockImplementation(() => {
      throw new Error("test error");
    });
    expect(() => utils.getLatestVersionOfHivePrint()).toThrow();
  });
});

describe("test checkIfUpdateIsNeeded", () => {
  it("should compare different versions", () => {
    expect(utils.checkIfUpdateIsNeeded("0.15.0", "0.15.1")).toBeTruthy();
    expect(utils.checkIfUpdateIsNeeded("0.15.0", "0.15.0")).toBeFalsy();
  });
});

describe("test update application", () => {
  // This doesn't really do anything of value. What we want to do is actually mock the whole thing now.
  it("should run updates", () => {
    const targetVersion = "latest";
    vi.mocked(execSync).mockImplementation((command) => {
      expect(command).toBe(`pnpm update @dovetail-digital/hiveprint-pi@latest`);
    });

    utils.updateApplication(targetVersion);
    expect(execSync).toHaveBeenCalledTimes(1);
  });

  it("should test try/catch for stop application", () => {
    vi.mocked(execSync, "execSync").mockImplementation(() => {
      throw new Error("test error");
    });
    expect(() => utils.updateApplication()).toThrow();
  });
});

describe("test stop application", () => {
  it("should stop the application", () => {
    vi.mocked(execSync).mockImplementation((command) => {
      expect(command).toBe("sudo systemctl stop hiveprintpi.service");
    });

    utils.stopApplication();
    expect(execSync).toHaveBeenCalledTimes(1);
  });

  it("should test try/catch for stop application", () => {
    vi.mocked(execSync, "execSync").mockImplementation(() => {
      throw new Error("test error");
    });
    expect(() => utils.stopApplication()).toThrow();
  });
});

describe("test restart application", () => {
  it("should restart the application", () => {
    vi.mocked(execSync).mockImplementation((command) => {
      expect(command).toBe("sudo systemctl restart hiveprintpi.service");
    });

    utils.restartApplication();
    expect(execSync).toHaveBeenCalledTimes(1);
  });

  it("should test try/catch for restart application", () => {
    vi.mocked(execSync, "execSync").mockImplementation(() => {
      throw new Error("test error");
    });
    expect(() => utils.restartApplication()).toThrow();
  });
});

describe("test the updater", () => {
  it("should not need update on latest", () => {
    vi.stubEnv("UPDATE_CHANNEL", "latest");
    const getInstalledVersionSpy = vi
      .spyOn(utils, "getInstalledVersionOfHivePrint")
      .mockReturnValueOnce("0.15.0");

    const getLatestVersionSpy = vi
      .spyOn(utils, "getLatestVersionOfHivePrint")
      .mockReturnValueOnce("0.15.0");

    const checkIfUpdateIsNeededSpy = vi.spyOn(utils, "checkIfUpdateIsNeeded");

    handleUpdates();

    expect(checkIfUpdateIsNeededSpy).toHaveBeenCalled();
    expect(checkIfUpdateIsNeededSpy).toHaveReturnedWith(false);
    expect(getInstalledVersionSpy).toHaveBeenCalledTimes(1);
    expect(getLatestVersionSpy).toHaveBeenCalledTimes(1);
  });

  it("should need update on latest", () => {
    vi.stubEnv("UPDATE_CHANNEL", "latest");
    const getInstalledVersionSpy = vi
      .spyOn(utils, "getInstalledVersionOfHivePrint")
      .mockReturnValue("0.15.0");

    const getLatestVersionSpy = vi
      .spyOn(utils, "getLatestVersionOfHivePrint")
      .mockReturnValue("0.15.1");

    const checkIfUpdateIsNeededSpy = vi.spyOn(utils, "checkIfUpdateIsNeeded");

    handleUpdates();

    expect(checkIfUpdateIsNeededSpy).toHaveBeenCalled();
    expect(checkIfUpdateIsNeededSpy).toHaveReturnedWith(true);
    expect(getInstalledVersionSpy).toHaveBeenCalledTimes(1);
    expect(getLatestVersionSpy).toHaveBeenCalledTimes(1);
    expect(execSync).toHaveBeenNthCalledWith(
      1,
      "pnpm update @dovetail-digital/hiveprint-pi@latest"
    );
    expect(execSync).toHaveBeenNthCalledWith(
      2,
      "sudo systemctl restart hiveprintpi.service"
    );
  });
  it("should handle try/catch", () => {
    vi.stubEnv("UPDATE_CHANNEL", "latest");
    const getInstalledVersionSpy = vi
      .spyOn(utils, "getInstalledVersionOfHivePrint")
      .mockImplementation(() => {
        throw new Error("test error");
      });

    expect(() => handleUpdates()).toThrow();
  });
});
