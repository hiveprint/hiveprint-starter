import { main } from "@dovetail-digital/hiveprint-pi";
import handleUpdates from "./updater.js";
const updateInterval = 60 * 5 * 1000; // check every 5 minutes.
setInterval(handleUpdates, updateInterval); // check every 24 hours
main();
