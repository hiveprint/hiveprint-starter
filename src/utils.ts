import { execSync } from "child_process";
import { execa } from "execa";
import inquirer from "inquirer";
import path from "path";
import { fileURLToPath } from "url";
import fs from "fs/promises";
import { HivePrintAnswers } from "./index.js";

const dovetailRegistryAuthentication =
  "//gitlab.com/api/v4/projects/28900673/packages/npm/:_authToken";
const dovetailRegistry =
  "@dovetail-digital:registry=https://gitlab.com/api/v4/projects/28900673/packages/npm/";

export const execCommand = (command: string) => {
  try {
    return execSync(command, { encoding: "utf8" });
  } catch (error) {
    console.log("Error", error);
    return null;
  }
};

export const getImportMetaUrl = () => import.meta.url;

export const getTemplateDir = () => {
  const importMetaUrl = getImportMetaUrl();
  const fileToPath = fileURLToPath(importMetaUrl);
  const dirname = path.dirname(fileToPath);
  const templateDir = path.resolve(dirname, "templates/standard");
  return templateDir;
};

export const getTargetDir = () => {
  const targetDir = path.resolve(process.cwd(), "hiveprint-pi");
  return targetDir;
};

/**
 *  Copy files from templates to hiveprint-pi
 */
export const copyFilesFromTemplateToHivePrintPi = async () => {
  console.log("Creating files from template...");

  const templateDir = getTemplateDir();
  const targetDir = getTargetDir();

  await execa("cp", ["-r", templateDir, targetDir]);
  console.log("Completes copy");
};

export const addAccessTokenToNpmrc = (accessToken: string) => {
  execSync(`pnpm config set ${dovetailRegistryAuthentication}=${accessToken}`);
  execSync(`pnpm config set ${dovetailRegistry}`);
};

export const setUpProject = async () => {
  await copyFilesFromTemplateToHivePrintPi();

  console.log("Adding HivePrint package...");
  await execa("pnpm", ["add", "@dovetail-digital/hiveprint-pi"], {
    cwd: "./hiveprint-pi",
  });
};

export const accessTokenExists = async () => {
  console.log("Checking for existing access token...");
  const existingConfig = execCommand(
    `pnpm config get ${dovetailRegistryAuthentication}`
  );
  return existingConfig;
};

export const updateConfigJsonWithUserInput = async (
  userInput: HivePrintAnswers
) => {
  const { printerIp, serverUrl } = userInput;
  const { stdout } = await execa("cat", ["config.json"], {
    cwd: "./hiveprint-pi/config",
  });
  let config = JSON.parse(stdout);
  config.ip = printerIp;
  config.server = serverUrl;
  const updatedConfig = JSON.stringify(config, null, 2);

  // Write updated JSON string back to file
  await fs.writeFile("./hiveprint-pi/config/config.json", updatedConfig);
};

export const askUserForAccessToken = async () => {
  return inquirer
    .prompt([
      {
        name: "accessToken",
        message: "Access token",
      },
    ])
    .then(async (answers: HivePrintAnswers) => {
      const { accessToken } = answers;
      return String(accessToken);
    })
    .catch((error: any) => {
      if (error.isTtyError) {
        console.log("Prompt couldn't be rendered in the current env.");
      } else {
        console.log("Something went wrong...", error);
      }
      return "";
    });
};

export const askUserForConfigJsonInput = async () => {
  return inquirer
    .prompt([
      { name: "printerIp", message: "Printer IP" },
      {
        name: "serverUrl",
        message: "Server URL (from team @ HivePrint)",
        validate: function (serverUrl) {
          const regex = /^https:\/\/[a-zA-Z0-9-]+\.hiveprint\.app/;
          return regex.test(serverUrl);
        },
      },
    ])
    .then(async (answers: HivePrintAnswers) => {
      return answers;
    });
};

export const uuidKeyExistsInConfig = async () => {
  try {
    const { stdout } = await execa("cat", ["config.json"], {
      cwd: "./hiveprint-pi/config/",
    });
    const config = JSON.parse(stdout);
    if ("uuid" in config) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};
