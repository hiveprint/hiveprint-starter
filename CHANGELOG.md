# @hiveprint/create-hiveprint-starter

## 1.30.18

### Patch Changes

- Fixes qidi port to be 10088 instead of 80

## 1.30.17

### Patch Changes

- updates service file with new env vars

## 1.30.16

### Patch Changes

- Adds env var for customizing the gcodes directory

## 1.30.15

### Patch Changes

- Adds .env for setting address

## 1.30.14

### Patch Changes

- 60b9022: Splits service files into different directories

## 1.30.13

### Patch Changes

- adds testing and local

## 1.30.12

### Patch Changes

- update to 5 min

## 1.30.11

### Patch Changes

- remove stop

## 1.30.10

### Patch Changes

- update interval for testing
- adds pi

## 1.30.9

### Patch Changes

- update build

## 1.30.8

### Patch Changes

- move view to info

## 1.30.7

### Patch Changes

- update console

## 1.30.6

### Patch Changes

- build

## 1.30.5

### Patch Changes

- update next

## 1.30.4

### Patch Changes

- update environment path

## 1.30.3

### Patch Changes

- update user to root in normal config

## 1.30.2

### Patch Changes

- updates root user

## 1.30.1

### Patch Changes

- supports legacy

## 1.30.0

### Minor Changes

- improves testing and refactors to new method

## 1.29.11

### Patch Changes

- adds latest hardcode

## 1.29.10

### Patch Changes

- testing

## 1.29.9

### Patch Changes

- updates version args

## 1.29.8

### Patch Changes

- increase build

## 1.29.7

### Patch Changes

- 1a92430: Changes update frequency
- 6497d70: Modifies to be 5 minutes
- af40d3a: adds build

## 1.29.7-next.2

### Patch Changes

- Modifies to be 5 minutes

## 1.29.7-next.1

### Patch Changes

- adds build

## 1.29.7-next.0

### Patch Changes

- Changes update frequency

## 1.29.6

### Patch Changes

- build

## 1.29.5

### Patch Changes

- adds sudo

## 1.29.4

### Patch Changes

- update build

## 1.29.3

### Patch Changes

- adds next systemctl file

## 1.29.2

### Patch Changes

- adds build

## 1.29.1

### Patch Changes

- Fixes the updater to grab array from json output

## 1.29.0

### Minor Changes

- Adds differential between next and latest channels

## 1.28.8

### Patch Changes

- Adds args

## 1.28.7

### Patch Changes

- updates instructions

## 1.28.6

### Patch Changes

- 7d7c1a5: removes await

## 1.28.5

### Patch Changes

- adds await

## 1.28.4

### Patch Changes

- change command around

## 1.28.3

### Patch Changes

- adds console log

## 1.28.2

### Patch Changes

- Adds proper variable name

## 1.28.1

### Patch Changes

- 7b27b0d: Adds changesets
- f0f158d: Adds new build and updater
